# packetCount

C & C++ compiling:

**g++ -x c -c main.c; g++ -std=c++11 occurrence.cpp main.o -lpcap -o packetCount**

  
Run tests for all files in directory:


**for i in ../testFiles/*; do ./packetCount $i; done > packetCount.log**


See outputs in *packetCount.log*
