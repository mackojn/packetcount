#include <pcap.h>
#include <net/ethernet.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

void occurPrint(void);
void occurUpdate(__time_t sec, char destIp[INET_ADDRSTRLEN]);

/******************************************************************************
 *   Process captured IP packet
 */
static void packetHandler(u_char *userData, const struct pcap_pkthdr* packetHeader, const u_char* packet)
{
  const uint16_t etherType = ntohs(((struct ether_header *) packet)->ether_type);

  if (etherType != ETHERTYPE_IP) {                                              // ignore all the other types
     printf("Unsupported ether_type = %04X\n", etherType);
     return;
  }

  const struct ip *ipHeader = (struct ip *) (packet + sizeof(struct ether_header));
  char destIp[INET_ADDRSTRLEN];

  inet_ntop(AF_INET, &(ipHeader->ip_dst), destIp, INET_ADDRSTRLEN);             // get IP

  occurUpdate(packetHeader->ts.tv_sec, destIp);                                 // take IP address and time stamp from packet to update statistics
}

/******************************************************************************
 *   Displays on the ...
 *
 *   argv[1] = Input File
 */
int main(int argc, char **argv) {
  if (argc < 2) {
    printf("Missing the input file.\n");
    return -1;
  }

  char errbuf[PCAP_ERRBUF_SIZE];
  pcap_t *pcap = pcap_open_offline(argv[1], errbuf);                            // create pcap handler, open capture file for offline processing

  if (pcap == NULL) {
    printf("Input file failure: %s\r\n", errbuf);
    return -1;
  }

  int linkLayerHeaderType = pcap_datalink(pcap);

  if (linkLayerHeaderType != DLT_EN10MB) {                                      // for the simplicity of example ignore all the other types
    printf("Unsupported Link Layer Header Type = %d\n", linkLayerHeaderType);
    return -1;
  }

  printf("###################################\r\n");
  printf("# File: %s\r\n", argv[1]);
  printf("###################################\r\n\n");

  if (pcap_loop(pcap, 0, packetHandler, NULL) < 0) {                            // start packet processing loop using packetHandler
    printf("Error: %s\r\n", pcap_geterr(pcap));
    return -1;
  }

  occurPrint();                                                                 // print the rest to close the last time period
  printf("\r\n\n\n\n\n");

  return 0;
}
