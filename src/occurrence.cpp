#include <netinet/ip.h>
#include <iostream>
#include <unordered_map>
#include <vector>
#include <algorithm>

using namespace std;

long int timeStamp = 0;                                                         // time value defining the current time interval
unordered_map<string, int> occurrence;                                          // map container with number of occurrences of all IP addresses
                                                                                // captured in the current interval

/******************************************************************************
 *   Prints statistics of the last 10 seconds
 *   sorted per occurrence of IP in descending order
 */
extern "C" void occurPrint(void) {
  cout << "Epoch Time: " << timeStamp << " - " << timeStamp + 10 << endl
       << "-----------------------------------" << endl;

  vector<pair<string, int>> v;                                                  // temporary use of vector container for sorting
                                                                                // copy unordered map container with "occurrences" into vector
  for (auto it = occurrence.begin(); it != occurrence.end(); it++)
  { v.push_back(make_pair(it->first, it->second)); }
                                                                                // sort "occurrences"
  sort(v.begin(), v.end(),
	   [] (const pair<string, int> &a, const pair<string, int> &b)
       { return (a.second == b.second) ? (a.first < b.first) : (a.second > b.second); });

  for (auto &e: v) { cout << e.second << "\t\t " << e.first << endl; }          // print in descending order

  if (v.size() == 0) cout << '0' << endl;
  cout << "===================================" << endl;
}

/******************************************************************************
 *   Updates "occurrences" of IP and
 *   prints statistics for the last time period
 *   sec - epoch time of sending
 *   destIp - IP address of the packet that was just sent
 */
extern "C" void occurUpdate(__time_t sec, char destIp[INET_ADDRSTRLEN]) {
  if (sec - timeStamp > 10) {                                                   // time period expired, it is time to print statistics
    long int newTimeStamp = 10 * (int) (sec / 10);

    if (timeStamp > 0) {
      occurPrint();                                                             // print statistics for the last active period
      occurrence.clear();                                                       // clear all "occurrences"

      while (newTimeStamp - timeStamp > 10) {                                   // print zero for all the periods of the silence
        timeStamp += 10;
        occurPrint();
      }
    }

    timeStamp = newTimeStamp;                                                   // set the start of the new time period
  }

  occurrence[destIp]++;                                                         // increase the number of occurrence of the recent captured IP address
}
